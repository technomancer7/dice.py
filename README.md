# Random Stuff
Some scripts to simulate some random things.

## Cards.py
A basic deck of cards simulator. Currently only supports standard playing cards, but the plan is for it to be expanded in to arbitrary formats.

## As application
Not yet implemented.

## As library

```py
import cards

deck = cards.Deck()
deck.shuffle() # Needed to randomize the list, otherwise will be in order.
h = deck.draw() # Removes top card from the list and returns it
print(h.value)
print(h.value_name)
print(h.suit)
print(h.suit_string)
print(h)
# 1 (int)
# "ace" (string, returns string of the value as a number if it's not a named card)
# Suits.CLUBS (enum)
# "clubs" (string)
# "ace of clubs" (string)
```

## Coin.py
The most simple coin flip. Nothing impressive but included anyway.

## As application
```sh
➜ ~/ coin.py
heads
```

## As library
```py
import coin
print(coin.coinFlip())
```

## Dice.py
A one-file command line application and library for dice rolling. That's it, nothing else, no dependencies, nothing fancy, just parses dice notation and gives an output.

### As application
```sh
➜ ~/ dice.py
Rolling 1d6 (+0)
Total: 1
Rolls: [1]
➜ ~/ dice.py help
Format: (fn)[die]d[sides](+mod)
Examples: d6 | 2d6 | 2d6+2
➜ ~/ dice.py
Rolling 1d6 (+0)
Total: 1
Rolls: [1]
➜ ~/ dice.py 10d6
Rolling 10d6 (+0)
Total: 42
Rolls: [4, 6, 4, 3, 6, 6, 4, 1, 2, 6]
➜ ~/ dice.py 10d6+10
Rolling 10d6 (+10)
Total: 144
Rolls: [14, 15, 15, 15, 11, 16, 13, 15, 16, 14]
```

### As library
```py
import dice

print(dice.parseDiceNotation("2d6"))
# {'sides': 6, 'modifier': 0, 'die': 2, 'valid': True}
print(dice.parseDiceNotation("d12"))
# {'sides': 12, 'modifier': 0, 'die': 1, 'valid': True}
print(dice.parseDiceNotation("54"))
# {'sides': 0, 'modifier': 0, 'die': 0, 'valid': False}
print(dice.roll("1d12+2"))
# {'rolls': [8], 'total': 8, 'sides': 12, 'modifier': 2, 'die': 1, 'valid': True}

result = dice.roll("6d10")
dice.prettyPrintRoll(result)
"""
Rolling 6d10 (+0)
Total: 39
Rolls: [1, 9, 10, 5, 5, 9]
"""
```
